﻿// <auto-generated />
using System;
using DoAnCoSo1.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace DoAnCoSo1.Migrations
{
    [DbContext(typeof(AppDBContext))]
    [Migration("20240403072231_Create")]
    partial class Create
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "8.0.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("DoAnCoSo1.Models.Account", b =>
                {
                    b.Property<int>("AccountId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("AccountId"));

                    b.Property<int?>("CommentId")
                        .HasColumnType("int");

                    b.Property<int?>("LikeId")
                        .HasColumnType("int");

                    b.Property<int?>("MessagesMessageId")
                        .HasColumnType("int");

                    b.Property<int?>("NoficationsNoficationId")
                        .HasColumnType("int");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("PostId")
                        .HasColumnType("int");

                    b.Property<int?>("RelationshipsRelationshipId")
                        .HasColumnType("int");

                    b.Property<int?>("ReportId")
                        .HasColumnType("int");

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("AccountId");

                    b.HasIndex("CommentId");

                    b.HasIndex("LikeId");

                    b.HasIndex("MessagesMessageId");

                    b.HasIndex("NoficationsNoficationId");

                    b.HasIndex("PostId");

                    b.HasIndex("RelationshipsRelationshipId");

                    b.HasIndex("ReportId");

                    b.ToTable("Accounts");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Class", b =>
                {
                    b.Property<int>("ClassId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("ClassId"));

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ClassId");

                    b.ToTable("Classes");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Comment", b =>
                {
                    b.Property<int>("CommentId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("CommentId"));

                    b.Property<string>("Content")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("timeStamp")
                        .HasColumnType("datetime2");

                    b.HasKey("CommentId");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Images", b =>
                {
                    b.Property<int>("ImagesId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("ImagesId"));

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Path")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("ImagesId");

                    b.ToTable("Images");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Like", b =>
                {
                    b.Property<int>("LikeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("LikeId"));

                    b.Property<DateTime>("timeStamp")
                        .HasColumnType("datetime2");

                    b.HasKey("LikeId");

                    b.ToTable("Likes");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Message", b =>
                {
                    b.Property<int>("MessageId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MessageId"));

                    b.Property<string>("Content")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("RecipientId")
                        .HasColumnType("int");

                    b.Property<int>("SenderId")
                        .HasColumnType("int");

                    b.Property<DateTime>("SentTime")
                        .HasColumnType("datetime2");

                    b.HasKey("MessageId");

                    b.ToTable("Messages");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Nofications", b =>
                {
                    b.Property<int>("NoficationId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("NoficationId"));

                    b.Property<int>("CommentId")
                        .HasColumnType("int");

                    b.Property<string>("Content")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("LikeId")
                        .HasColumnType("int");

                    b.Property<int>("MessageId")
                        .HasColumnType("int");

                    b.HasKey("NoficationId");

                    b.HasIndex("CommentId")
                        .IsUnique();

                    b.HasIndex("LikeId")
                        .IsUnique();

                    b.HasIndex("MessageId")
                        .IsUnique();

                    b.ToTable("Nofications");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Post", b =>
                {
                    b.Property<int>("PostId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("PostId"));

                    b.Property<int>("ClassId")
                        .HasColumnType("int");

                    b.Property<int>("CommentId")
                        .HasColumnType("int");

                    b.Property<string>("Content")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ImagesId")
                        .HasColumnType("int");

                    b.Property<int>("LikeCount")
                        .HasColumnType("int");

                    b.Property<int>("LikeId")
                        .HasColumnType("int");

                    b.Property<int>("SubjectId")
                        .HasColumnType("int");

                    b.Property<int>("commentCount")
                        .HasColumnType("int");

                    b.Property<string>("timeStamp")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("PostId");

                    b.HasIndex("ClassId")
                        .IsUnique();

                    b.HasIndex("CommentId");

                    b.HasIndex("ImagesId");

                    b.HasIndex("LikeId");

                    b.HasIndex("SubjectId")
                        .IsUnique();

                    b.ToTable("Posts");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Relationship", b =>
                {
                    b.Property<int>("RelationshipId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("RelationshipId"));

                    b.Property<int>("Account1Id")
                        .HasColumnType("int");

                    b.Property<int>("Account2Id")
                        .HasColumnType("int");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("RelationshipId");

                    b.ToTable("Relationships");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Report", b =>
                {
                    b.Property<int>("ReportId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("ReportId"));

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("PostId")
                        .HasColumnType("int");

                    b.HasKey("ReportId");

                    b.HasIndex("PostId")
                        .IsUnique();

                    b.ToTable("Report");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Subject", b =>
                {
                    b.Property<int>("SubjectId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("SubjectId"));

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("SubjectId");

                    b.ToTable("Subjects");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.User", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("UserId"));

                    b.Property<int>("AccountId")
                        .HasColumnType("int");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FullName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("Sex")
                        .HasColumnType("bit");

                    b.HasKey("UserId");

                    b.HasIndex("AccountId")
                        .IsUnique();

                    b.ToTable("Users");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Account", b =>
                {
                    b.HasOne("DoAnCoSo1.Models.Comment", "Comment")
                        .WithMany("Accounts")
                        .HasForeignKey("CommentId");

                    b.HasOne("DoAnCoSo1.Models.Like", "Like")
                        .WithMany("Accounts")
                        .HasForeignKey("LikeId");

                    b.HasOne("DoAnCoSo1.Models.Message", "Messages")
                        .WithMany("Accounts")
                        .HasForeignKey("MessagesMessageId");

                    b.HasOne("DoAnCoSo1.Models.Nofications", "Nofications")
                        .WithMany("Accounts")
                        .HasForeignKey("NoficationsNoficationId");

                    b.HasOne("DoAnCoSo1.Models.Post", "Post")
                        .WithMany("Account")
                        .HasForeignKey("PostId");

                    b.HasOne("DoAnCoSo1.Models.Relationship", "Relationships")
                        .WithMany("Accounts")
                        .HasForeignKey("RelationshipsRelationshipId");

                    b.HasOne("DoAnCoSo1.Models.Report", "Report")
                        .WithMany("Accounts")
                        .HasForeignKey("ReportId");

                    b.Navigation("Comment");

                    b.Navigation("Like");

                    b.Navigation("Messages");

                    b.Navigation("Nofications");

                    b.Navigation("Post");

                    b.Navigation("Relationships");

                    b.Navigation("Report");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Nofications", b =>
                {
                    b.HasOne("DoAnCoSo1.Models.Comment", "comment")
                        .WithOne("Nofications")
                        .HasForeignKey("DoAnCoSo1.Models.Nofications", "CommentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DoAnCoSo1.Models.Like", "like")
                        .WithOne("Nofications")
                        .HasForeignKey("DoAnCoSo1.Models.Nofications", "LikeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DoAnCoSo1.Models.Message", "message")
                        .WithOne("Nofications")
                        .HasForeignKey("DoAnCoSo1.Models.Nofications", "MessageId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("comment");

                    b.Navigation("like");

                    b.Navigation("message");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Post", b =>
                {
                    b.HasOne("DoAnCoSo1.Models.Class", "Class")
                        .WithOne("Post")
                        .HasForeignKey("DoAnCoSo1.Models.Post", "ClassId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DoAnCoSo1.Models.Comment", "Comment")
                        .WithMany("Posts")
                        .HasForeignKey("CommentId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DoAnCoSo1.Models.Images", "Images")
                        .WithMany("Posts")
                        .HasForeignKey("ImagesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DoAnCoSo1.Models.Like", "Like")
                        .WithMany("Posts")
                        .HasForeignKey("LikeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("DoAnCoSo1.Models.Subject", "Subject")
                        .WithOne("Post")
                        .HasForeignKey("DoAnCoSo1.Models.Post", "SubjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Class");

                    b.Navigation("Comment");

                    b.Navigation("Images");

                    b.Navigation("Like");

                    b.Navigation("Subject");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Report", b =>
                {
                    b.HasOne("DoAnCoSo1.Models.Post", "Post")
                        .WithOne("Report")
                        .HasForeignKey("DoAnCoSo1.Models.Report", "PostId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Post");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.User", b =>
                {
                    b.HasOne("DoAnCoSo1.Models.Account", "Account")
                        .WithOne("User")
                        .HasForeignKey("DoAnCoSo1.Models.User", "AccountId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Account");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Account", b =>
                {
                    b.Navigation("User");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Class", b =>
                {
                    b.Navigation("Post");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Comment", b =>
                {
                    b.Navigation("Accounts");

                    b.Navigation("Nofications");

                    b.Navigation("Posts");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Images", b =>
                {
                    b.Navigation("Posts");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Like", b =>
                {
                    b.Navigation("Accounts");

                    b.Navigation("Nofications");

                    b.Navigation("Posts");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Message", b =>
                {
                    b.Navigation("Accounts");

                    b.Navigation("Nofications");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Nofications", b =>
                {
                    b.Navigation("Accounts");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Post", b =>
                {
                    b.Navigation("Account");

                    b.Navigation("Report");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Relationship", b =>
                {
                    b.Navigation("Accounts");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Report", b =>
                {
                    b.Navigation("Accounts");
                });

            modelBuilder.Entity("DoAnCoSo1.Models.Subject", b =>
                {
                    b.Navigation("Post");
                });
#pragma warning restore 612, 618
        }
    }
}
