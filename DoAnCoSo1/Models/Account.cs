﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DoAnCoSo1.Models
{
    public class Account
    {
        [Key]
        public int AccountId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        //
        [JsonIgnore]
        public User? User { get; set; }

        [JsonIgnore]
        public Message? Messages { get; set; }


        [JsonIgnore]
        public Relationship? Relationships { get; set; }
        [JsonIgnore]
        public Post? Post { get; set; }
        [JsonIgnore]
        public Like? Like { get; set; }
        [JsonIgnore]
        public Comment? Comment { get; set; }
        public Nofications? Nofications { get; set;}
        public Report? Report { get; set; }
    }
}
