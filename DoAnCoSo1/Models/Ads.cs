﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSo1.Models
{
    public class Ads
    {
        [Key]
        public int AdsId { get; set; }
        public string AdsPath { set; get; }
        public string AdsImage { set; get; }
    }
}
