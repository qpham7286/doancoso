﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DoAnCoSo1.Models
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }
        public string Content { get; set; }
        public DateTime timeStamp { get; set; }
        [JsonIgnore]
        public List<Account> Accounts { get; set; }
        public List<Post> Posts { get; set; }
        public Nofications? Nofications { get; set; }

    }
}
