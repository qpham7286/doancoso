﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DoAnCoSo1.Models
{
    public class Images
    {
        [Key]
        public int ImagesId { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }
        [JsonIgnore]
        public List<Post> Posts { get; set; }
    }
}
