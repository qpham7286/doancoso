﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DoAnCoSo1.Models
{
    public class Like
    {
        [Key]
        public int LikeId { get; set; }
        public DateTime timeStamp { get; set; }
        [JsonIgnore]
        public List<Account>? Accounts { get; set; }
        [JsonIgnore]
        public List<Post>?  Posts { get; set; }
        public Nofications? Nofications { get; set; }
    }
}
