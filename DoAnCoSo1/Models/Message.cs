﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DoAnCoSo1.Models
{
    public class Message
    {
        [Key]
        public int MessageId { get; set; }
        public string Content { get; set; }
        // Tài khoản người gửi
        public int SenderId { get; set; }
        // Tài khoản người nhận
        public int RecipientId { get; set; }
        public List<Account>? Accounts { get; set; }

        public DateTime SentTime { get; set; }
        public Nofications? Nofications { get; set; }
    }
}
