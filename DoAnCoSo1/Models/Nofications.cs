﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSo1.Models
{
    public class Nofications
    {
        [Key]
        public int NoficationId { set; get; }
        public string Content { set; get; }
        public Like? like { set; get; }
        public int LikeId { set; get; }
        public Comment? comment { set; get; }
        public int CommentId { set; get; }
        public Message? message { set; get; }
        public int MessageId { set; get; }
        public List<Account>? Accounts { set; get; }
    }
}
