﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DoAnCoSo1.Models
{
    public class Post
    {
        [Key]
        public int PostId { get; set; }
        public string Content { get; set; }
        public string timeStamp { get; set; }
            
        public int LikeCount { get; set; }
        public int commentCount { get; set; }
        [JsonIgnore]
        public List<Account>? Account { get; set; }
        [JsonIgnore]
        public  Like? Like { get; set;}
        public int LikeId { get; set; }
        public Comment? Comment { get; set; }
        public int CommentId {  get; set; }
        [JsonIgnore]
        public Images? Images { get; set; }
        public int ImagesId { get; set; }
        [JsonIgnore]
        public Class? Class { get; set; }
        public int ClassId { get; set; }
        [JsonIgnore]
        public Subject? Subject { get; set; }
        public int SubjectId { get; set; }
        public Report? Report { get; set; }
    }
}
