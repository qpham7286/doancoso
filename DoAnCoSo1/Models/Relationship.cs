﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DoAnCoSo1.Models
{
    public class Relationship
    {
        [Key]
        public int RelationshipId { get; set; }

        // Id của Account 1
        public int Account1Id { get; set; }
        // Id của Account 2
        public int Account2Id { get; set; }
        public List<Account>? Accounts { get; set; }
       
        // Mô tả mối quan hệ
        public string Description { get; set; }
    } 
}
