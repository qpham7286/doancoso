﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSo1.Models
{
    public class Report
    {
        [Key]
        public int ReportId { get; set; }
        public string Description { get; set; }
        public List<Account> Accounts { get; set;}
        public Post? Post { get; set; }
        public int PostId { get; set; }
    }
}
