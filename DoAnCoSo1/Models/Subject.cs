﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DoAnCoSo1.Models
{
    public class Subject
    {
        [Key]
        public int SubjectId { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public Post? Post { get; set; }
    }
}
