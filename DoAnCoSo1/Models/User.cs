﻿using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DoAnCoSo1.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public DateTime Date { get; set; }
        public bool Sex { get; set; }
        public int AccountId { get; set; }
        [JsonIgnore]
        public Account Account{ get; set; }
    }
}
